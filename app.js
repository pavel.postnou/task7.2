const express = require("express");
const userRouter = require("./user.js");
const admRouter = require("./admin.js");
const app = express();
app.use("/persons",userRouter,admRouter);
app.use("/persons", function (request,response){
    response.send("<h1>Выбор профиля</h1>")
})
app.use("/", function (request,response){
    response.send("<h1>Главная страница</h1>")
})
app.listen(3000);