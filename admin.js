const express = require("express");
const admParser = express.json();
const admRouter = express.Router();

admRouter.post("/admin/create",admParser, function(request, response){
    global.admin = { 
        id: request.body.id, 
        name: request.body.name 
    };
    response.send(`<h3>Данные внесены ${global.admin.name} ${global.admin.id}</h3>`);
});

admRouter.get("/admin/:id",admParser, function(request, response){
    response.send("<h1>id админа</h1><p>id=" + global.admin.id);
});

admRouter.put("/admin/:id",admParser, function(request, response){
    response.send("<h1>Обновление данных админа</h1><p>id=" + global.admin.id);
});

admRouter.delete("/admin/:id", admParser, function(request, response){
    response.send("<h1>Удаление админа</h1><p>id=" + global.admin.id);
});
 
module.exports = admRouter;